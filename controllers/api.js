const _ = require('lodash');
const countries = require('../resources/countries');

const notFound = (res) => {
  res.json(404, {
    message: "Sorry, that page does not exist",
    code: 34
  })
}


exports.index = (req, res) => {
  res.send({ message: "Hey buddy, check all countries by hitting /all"})
}


exports.getAllCountriesAndDetails = (req, res) => {
  res.send({ data: countries });
}

exports.getCountryCode = (req, res) => {
  const { query } = req;
  const { country } = query;

  if (!query || !country) res.send({ message: "Invalid country" });

  const all = country.replace(/ /g,'').split(',');
  console.log('countries', all);

  let finalObj = {};


  all.forEach((item, index) => {
    if(!item) return;

    const findCode = c => {
      if(c.name.common.toLowerCase() === item.toLowerCase()){
        finalObj = {
          ...finalObj,
          [item]: [c.cca2, c.cca3]
        }
      }
    }

    _.find(countries, findCode);
  })


  if (!_.size(finalObj)) res.send({ message: "Invalid country" });

  res.send({ data: finalObj });
}
