const express = require('express');


var app = express();
var port = process.env.PORT || 3000;

const API = require('./controllers/api');


// use the express-static middleware
app.use(express.static("public"))


app.get("/", API.index);

app.get("/getAll", API.getAllCountriesAndDetails);

/**
 * accept both string or comma seperated string
 *
 * /getCode?country=afghanistan
 * /getCode?country=afghanistan,germany
 *
 */
app.get("/getCode", API.getCountryCode);


app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

module.exports = app;
