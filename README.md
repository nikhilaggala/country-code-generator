/**
 * accept both string or comma seperated string
 *
 * /getCode?country=afghanistan
 * /getCode?country=afghanistan,germany
 *
 */

 returns

 {
    "data": {
        "afghanistan": [
            "AF",
            "AFG"
        ]
    }
}

or 

{
    "data": {
        "afghanistan": [
            "AF",
            "AFG"
        ],
        "germany": [
            "DE",
            "DEU"
        ]
    }
}
